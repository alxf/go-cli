#
# Makefile to manage the cli package
#

go-build = go build
go-generate = go generate
go-run = go run
go-test = richgo test
go-fmt = gofmt -l -w

GENERATED_FILES = optstruct.go \
                  optinterface.go \
                  optlookup_command.go \
                  optlookup_subcommand.go

default: all

all: generate tests

.PHONY: generate
generate:
	@rm -f $(GENERATED_FILES)
	@echo generate source files...
	@$(go-generate)
	@$(go-fmt) $(GENERATED_FILES)

.PHONY: tests
tests:
	@$(go-test) -v
