go-cli - command line parser package
====================================

A command line parser in GO, heavily inspired by the Python **argparse** module.

-------------------------------------------------------------------------------

__Table of Contents__

- [Getting started](#getting-started)
- [Features](#features)
- [Basic example](#basic-example)
- [Documentation](#documentation)
  - [The Command structure](#the-command-structure)
  - [Define options](#define-options)
  - [Define exclusive group of options](#define-exclusive-group-of-options)
  - [Define a subcommand parser](#define-a-subcommand-parser)
  - [Parse the command line](#parse-the-command-line)
  - [Retrieve values](#retrieve-values)
- [Run the tests](#run-the-tests)
- [License](#license)

-------------------------------------------------------------------------------

## Getting started

In order to use this package, you will need to get a copy of it:

``` shellsession
$ go get gitlab.com/alxf/go-cli
```

## Features

- autogenerated program `--help, -h`
- autogenerated program `--version`
- option type validation
- mandatory options
- custom default option values
- option alias
- validate answer against a list of choices
- exclusives options

## Basic example

``` go
package main

import (
    "gitlab.com/alxf/go-cli"
)

func main() {
    var (
        config string
        debug  bool
    )

    cmd := cli.NewCommand("myprogram", "my awesome program", "1.0")

    if err := cmd.AddOption("--debug", &OptBool{}); err != nil {
        panic(err)
    }

    if err := cmd.AddOption("--config", &OptString{}); err != nil {
        panic(err)
    }

    cmd.ParseOrExit()

    cmd.LookupStringVar("--config", &config)
    cmd.LookupBoolVar("--debug", &debug)
}

```

## Documentation

### The Command structure

``` go
type Command struct {
    Program     string
    Description string
    Version     string
    Args        []string
}
```

The `Command` structure will hold the command line parser definition, and
be used to retrieve the options values when needed.

It can be created with the `NewCommand` function, which take some arguments
to setup the parser like the program name.

``` go
func NewCommand(program, description, version string) *Command
```

- *program*: the program name, or let it empty to generate the name from the
  binary path.

- *description*: the program description, displayed in the generated help,
  let it empty to don't display any description.

- *version*: the program version, generate a `--version` switch, let it
  empty to disable it.

### Define options

Options can be added to the command line parser with the `AddOption` method
which take the option name, and a structure to define it as arguments. The
option structure should implement the interface `IOption`.

``` go
func (c *Command) AddOption(name string, opt IOption) error
```

The package define several type of options, which correspond to go types:

* `OptBool`
* `OptString`
* `OptInt`, `OptInt8`, `OptInt16`, `OptInt32`, `OptInt64`
* `OptUint`, `OptUint8`, `OptUint16`, `OptUint32`, `OptUint64`
* `OptFloat32`, `OptFloat64`
* `OptIP`

If the given value doesn't match the type, an error will be returned.

Options behavior can be controlled through the structure fields. Let's assume
i want to add an option which will be mandatory, of type string.

``` go
err := cmd.AddOption("--config", &OptString{
    Mandatory: true,
    Usage:     "set the path of the configuration file",
})
if err != nil {
    panic(err)
}

```

All type of options have these fields:

- *Choices*: list of valid choices for this option (empty to disable)
- *Default*: the default value for this option
- *Mandatory*: indicate if the option must be set
- *Usage*: the option definition to print in the generated usage

### Define exclusive group of options

If some of your options can't be used together, you can define one or more
group of exclusive options with the `AddExclusiveGroup` method.

``` go
func (c *Command) AddExclusiveGroup(args ...string) error
```

Just give a list of option names, or aliases, after defining them to register
a group of exclusive options.

### Define a subcommand parser

To add a subcommand to the command parser you will need to use the
`AddSubCommand` method.

``` go
func (c *Command) AddSubCommand(name, description string, alias ...string) (*SubCommand, error)
```

You can then use the `SubCommand` pointer to add option like with the main
command line parser.

``` go
sc, err := cmd.AddSubCommand("start", "start the service", "s")
if err != nil {
    panic(err)
}

if err := sc.AddOption("--restart", &OptBool{
        Aliases: []string{"-r"},
        Usage: "if the service is already started, restart it",
    }); err != nil {
    panic(err)
}
```

To retrieve the value of the subcommand, once the command line is parsed, you will
need to call the `GetCommand` method from the command line parser, this method will
return the name of the triggered command if any, or empty string and a pointer on
the `SubCommand` struct triggered or nil.

``` go
cmd.ParseOrExit()

name, sc := cmd.GetCommand()
if name == "start" {
    var restart bool
    sc.LookupBoolVar("--restart", &restart)
}
```

### Parse the command line

Once you have setup the command line parser, there is two ways to parse
input:

- The `Command.Parse()` method, will let you manage the error
- The `Command.ParseOrExit()` will exit when appropriate and print an
  error message on the stderr for you

### Retrieve values

To retrieve an option value, you need to call the appropriate
`Command.Lookup<Type>Var`, you have for each type of options the
matching method.

For example, for the previous example we could use this:

``` go
var config string

cmd.LookupStringVar("--config", &config)
```

The variable will be set only if the option exists.

## Run the tests

In order to run the tests you will need to install richgo:

``` shellsession
$ go get -u github.com/kyoh86/richgo
```

If you want to run the tests for the package:

``` shellsession
$ make tests
```

## License

This project is licensed under the [WTFPL](http://wtfpl.net), see the *LICENSE*
file for details.
