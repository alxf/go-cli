// Package cli Go command line parser
package cli

import (
	"fmt"
	"os"
	"path"
	"strconv"
)

// Command structure which hold a command line parser definition
type Command struct {
	Program     string
	Description string
	Version     string
	Args        []string
	options     map[string]IOption
	exclusives  [][]string
	command     string
	subcommands map[string]*SubCommand
}

// NewCommand create a new command line parser
func NewCommand(program, description, version string) *Command {
	progname := path.Base(os.Args[0])

	if program != "" {
		progname = program
	}

	return &Command{
		Program:     progname,
		Description: description,
		Version:     version,
		Args:        make([]string, 0),
		options:     make(map[string]IOption),
		exclusives:  make([][]string, 0),
		subcommands: make(map[string]*SubCommand),
	}
}

// Command implementation //////////////////////////////////////////////////////

// AddOption `Command` method to add an option to the parser
func (c *Command) AddOption(name string, opt IOption) error {
	if _, ok := c.options[name]; ok {
		return &OptionError{Kind: OptionAlreadyExists, Name: name}
	}

	if err := opt.SetValue(opt.GetDefault()); err != nil {
		return err
	}
	opt.SetSeen(false) // we don't want to trigger the option yet
	c.options[name] = opt

	return nil
}

// AddExclusiveGroup `Command` method to add a group of exclusive
// options by name or alias
func (c *Command) AddExclusiveGroup(args ...string) error {
	if len(args) < 2 {
		return fmt.Errorf(
			"exclusive group need at least 2 options to be useful",
		)
	}

	group := make([]string, 0)
	for _, opt := range args {
		// validate each option name before adding it to the group
		name := c.findOptionKey(opt)
		if name == "" {
			return fmt.Errorf("invalid option name '%v'", opt)
		}
		group = append(group, name)
	}
	c.exclusives = append(c.exclusives, group)

	return nil
}

// AddSubCommand `Command` method to create a sub command parser
func (c *Command) AddSubCommand(
	name, description string, aliases ...string) (*SubCommand, error) {
	if _, ok := c.subcommands[name]; ok {
		return nil, &OptionError{Kind: OptionAlreadyExists, Name: name}
	}

	c.subcommands[name] = &SubCommand{
		Program:     c.Program,
		Aliases:     aliases,
		Description: description,
		options:     make(map[string]IOption),
	}

	return c.subcommands[name], nil
}

//go:generate ./cligen.py lookup Command

// Usage generated usage from the command line parser definition
func (c *Command) Usage() {
	fmt.Printf("usage: %s [options] [arguments] ... \n\n", c.Program)

	if c.Description != "" {
		fmt.Println("description:")
		fmt.Printf("  %s\n\n", c.Description)
	}

	fmt.Println("options:")
	fmt.Println("  --help, -h              show this help message and exit")
	if c.Version != "" {
		fmt.Println("  --version               display the program version")
	}

	for name, opt := range c.options {
		fmt.Printf("%s\n", formatOption(name, opt))
	}

	fmt.Printf("\n")
}

// GetCommand return the selected command name and the sub command pointer or
// empty string and nil if no command was triggered.
func (c *Command) GetCommand() (string, *SubCommand) {
	if c.command != "" {
		return c.command, c.subcommands[c.command]
	}

	return "", nil
}

// Command helpers /////////////////////////////////////////////////////////////

// findOptionKey `Command` method to retrieve an option key given
// the name or an alias
func (c *Command) findOptionKey(name string) string {
	if _, ok := c.options[name]; ok {
		return name
	}

	// search in aliases
	for k, v := range c.options {
		for _, s := range v.GetAliases() {
			if s == name {
				return k
			}
		}
	}

	return ""
}

// checkExclusiveGroups `Command` method which check if two or more options of
// the same exclusive group are triggered together
func (c *Command) checkExclusiveGroups() error {
	for _, group := range c.exclusives {
		found := false
		for _, opt := range group {
			if c.options[opt].GetSeen() && !found {
				found = true
			} else if c.options[opt].GetSeen() {
				return &OptionError{
					Kind:    OptionExclusive,
					ExcList: group,
					Name:    opt,
				}
			}
		}
	}

	return nil
}

// checkMandatoryOptions `Command` method which check for each mandatory
// option if it was given on the command line or not
func (c *Command) checkMandatoryOptions() error {
	for k, v := range c.options {
		if v.GetMandatory() && !v.GetSeen() {
			return &OptionError{Kind: OptionMandatory, Name: k}
		}
	}

	return nil
}

// setOption `Command` method to set the option value for the given key
func (c *Command) setOption(name string, value string) error {
	opt, ok := c.options[name]
	if !ok {
		return &OptionError{Kind: OptionNotFound, Name: name}
	}
	opt.SetSeen(true)
	if err := opt.SetValue(value); err != nil {
		switch err.(*ValueError).Kind {
		case InvalidValue:
			return &OptionError{Kind: OptionInvalidValue, Name: name}
		case InvalidChoice:
			return &OptionError{Kind: OptionInvalidChoice, Name: name}
		case UnexpectedValue:
			return &OptionError{Kind: OptionInternalError, Err: err}
		}
	}

	return nil
}

// setBoolOption `Command` method to set bool option only with the opposite
// of the default value stored as string
func (c *Command) setBoolOption(key string) error {
	opt, ok := c.options[key]
	if !ok {
		return &OptionError{Kind: OptionNotFound, Name: key}
	}
	b, err := strconv.ParseBool(opt.GetDefault())
	if err != nil {
		// conside result as false value
		b = false
	}
	opt.SetSeen(true)
	if err := opt.SetValue(!b); err != nil {
		switch err.(*ValueError).Kind {
		case InvalidValue:
			return &OptionError{Kind: OptionInvalidValue, Name: key}
		case UnexpectedValue:
			return &OptionError{Kind: OptionInternalError, Err: err}
		}
	}

	return nil
}
