#!/usr/bin/env python3

""" Script to generate cli package option types related code """

import sys
from collections import namedtuple


OptionType = namedtuple('OptionType', "kind, gotype, default")

typemap = [
    OptionType("Bool", "bool", "false"),
    OptionType("Int", "int", "-1"),
    OptionType("Int8", "int8", "-1"),
    OptionType("Int16", "int16", "-1"),
    OptionType("Int32", "int32", "-1"),
    OptionType("Int64", "int64", "-1"),
    OptionType("Uint", "uint", "0"),
    OptionType("Uint8", "uint8", "0"),
    OptionType("Uint16", "uint16", "0"),
    OptionType("Uint32", "uint32", "0"),
    OptionType("Uint64", "uint64", "0"),
    OptionType("String", "string", "\"\""),
    OptionType("Float32", "float32", "0.0"),
    OptionType("Float64", "float64", "0.0"),
    OptionType("IP", "net.IP", "127.0.0.1"),
]

HEADER_STRUCT = """\
package cli

import (
    "net"
)
"""

TEMPLATE_STRUCT = """\
// Opt{0.kind} ...
type Opt{0.kind} struct {{
    Aliases   []string
    Choices   []string
    Default   string
    Mandatory bool
    Usage     string
    seen      bool
    value     {0.gotype}
}}

"""

HEADER_INTERFACE = """\
package cli

"""

TEMPLATE_INTERFACE = """\
func (o *Opt{0.kind}) GetAliases() []string {{
    return o.Aliases
}}

func (o *Opt{0.kind}) isValidChoice(choice string) bool {{
    if len(o.Choices) < 1 {{
        return true
    }}

    found := false
    for _, e := range o.Choices {{
        if e == choice {{
            found = true
            break
        }}
    }}

    return found
}}

func (o *Opt{0.kind}) GetDefault() string {{
    return o.Default
}}

func (o *Opt{0.kind}) GetMandatory() bool {{
    return o.Mandatory
}}

func (o *Opt{0.kind}) GetSeen() bool {{
    return o.seen
}}

func (o *Opt{0.kind}) GetUsage() string {{
    return o.Usage
}}

func (o *Opt{0.kind}) GetValue() interface{{}} {{
    return o.value
}}

func (o *Opt{0.kind}) SetSeen(value bool) {{
    o.seen = value
}}

"""

HEADER_LOOKUP = """\
package cli

import (
    "net"
)
"""

TEMPLATE_LOOKUP = """\
func (c *{1}) Lookup{0.kind}Var(key string, value *{0.gotype}) {{
    opt, ok := c.options[key]
    if !ok {{
        return
    }}
    *value = opt.GetValue().({0.gotype})
}}

"""


if __name__ == "__main__":
    content = []

    if len(sys.argv) <= 1:
        print("[error] missing arguments")
        quit(2)

    if sys.argv[1].lower() == "struct":
        filename = "optstruct.go"
        content.append(HEADER_STRUCT)
        for opt in typemap:
            content.append(TEMPLATE_STRUCT.format(opt))
    elif sys.argv[1].lower() == "interface":
        filename = "optinterface.go"
        content.append(HEADER_INTERFACE)
        for opt in typemap:
            content.append(TEMPLATE_INTERFACE.format(opt))
    elif sys.argv[1].lower() == "lookup":
        if len(sys.argv) <= 2:
            print("[error] missing arguments")
            quit(2)
        filename = "optlookup_{}.go".format(sys.argv[2].lower())
        content.append(HEADER_LOOKUP)
        for opt in typemap:
            content.append(TEMPLATE_LOOKUP.format(opt, sys.argv[2]))
    else:
        print("[error] unknown command")
        quit(2)

    with open(filename, 'w+') as fd:
        for line in content:
            fd.write("{}\n".format(line.replace('    ', '\t')))

    quit()
