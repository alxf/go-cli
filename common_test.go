package cli

import (
	"testing"
)

// Test: formatOption //////////////////////////////////////////////////////////

type inputFormatOption struct {
	name     string
	opt      IOption
	expected string
}

func TestFormatOption(t *testing.T) {
	var inputTests = []inputFormatOption{
		{
			"--debug",
			&OptBool{Aliases: []string{"-d"}, Usage: "debug option"},
			"  --debug, -d             debug option",
		},
		{
			"--config",
			&OptString{Aliases: []string{"-c"}, Usage: "config option"},
			"  --config=VALUE, -c      config option",
		},
		{
			"-s",
			&OptString{Usage: "short string option"},
			"  -s VALUE                short string option",
		},
		{
			"--int",
			&OptInt{Usage: "integer option"},
			"  --int=NUM               integer option",
		},
		{
			"--uint",
			&OptUint{Usage: "unsigned integer option"},
			"  --uint=NUM              unsigned integer option",
		},
		{
			"--float",
			&OptFloat32{Usage: "float number option"},
			"  --float=FLOAT           float number option",
		},
		{
			"--float64",
			&OptFloat64{Usage: "float64 number option"},
			"  --float64=FLOAT         float64 number option",
		},
		{
			"--very-long-option",
			&OptString{
				Aliases: []string{"-v", "-l", "-o"},
				Usage: "test when option labels are longer than the tab " +
					"and usage verry verry lonng",
			},
			"  --very-long-option=VALUE, -v, -l, -o\n" +
				"                          " +
				"test when option labels are longer than the tab and\n" +
				"                          " +
				"usage verry verry lonng",
		},
	}

	for _, input := range inputTests {
		result := formatOption(input.name, input.opt)
		assertEq(t, result, input.expected)
	}
}

// Test: splitLongOption ///////////////////////////////////////////////////////

type inputSplitLongOption struct {
	value string
	left  string
	right string
}

func TestSplitLongOption(t *testing.T) {
	var inputTests = []inputSplitLongOption{
		{"--long-option=value", "--long-option", "value"},
		{"--long-option=value=value", "--long-option", "value=value"},
		{"-abcdef", "", ""},
		{"-c=arg", "", ""},
	}

	for _, input := range inputTests {
		left, right := splitLongOption(input.value)
		assertEq(t, left, input.left)
		assertEq(t, right, input.right)
	}
}
