package cli

import (
	"fmt"
)

// optionErrorKind unique type of OptionError
type optionErrorKind int

// OptionError enum kinds
const (
	OptionAlreadyExists optionErrorKind = iota
	OptionExclusive
	OptionNotFound
	OptionMandatory
	OptionMissingArgument
	OptionInvalidChoice
	OptionInvalidValue
	OptionInternalError
	OptionUnexpectedType
)

// OptionError structure which hold an option error
type OptionError struct {
	Kind    optionErrorKind
	Name    string
	ExcList []string
	Err     error
}

// OptionError implementation of Error interface
func (e *OptionError) Error() string {
	switch e.Kind {
	case OptionAlreadyExists:
		return fmt.Sprintf("option '%v' already exists", e.Name)
	case OptionExclusive:
		return fmt.Sprintf(
			"exclusive option '%v' can't be used with: %v", e.Name, e.ExcList,
		)
	case OptionNotFound:
		return fmt.Sprintf("no such option '%v'", e.Name)
	case OptionMandatory:
		return fmt.Sprintf("missing mandatory option '%v'", e.Name)
	case OptionMissingArgument:
		return fmt.Sprintf("missing argument for option '%v'", e.Name)
	case OptionInvalidChoice:
		return fmt.Sprintf("invalid choice for option '%v'", e.Name)
	case OptionInvalidValue:
		return fmt.Sprintf("invalid value for option '%v'", e.Name)
	case OptionInternalError:
		return fmt.Sprintf("an internal problem occurs (%v)", e.Err)
	case OptionUnexpectedType:
		return fmt.Sprintf("unknown type for option '%v'", e.Name)
	}

	panic("never reached")
}

// valueErrorKind unique type of ValueError
type valueErrorKind int

// ValueError enum kinds
const (
	InvalidValue valueErrorKind = iota
	InvalidChoice
	UnexpectedValue
)

// ValueError structure which hold a value error
type ValueError struct {
	Kind  valueErrorKind
	Value interface{}
}

// ValueError implementation of Error interface
func (e *ValueError) Error() string {
	switch e.Kind {
	case InvalidValue:
		return fmt.Sprintf("can't convert value (%v)", e.Value)
	case InvalidChoice:
		return fmt.Sprintf("not a valid choice (%v)", e.Value)
	case UnexpectedValue:
		return fmt.Sprintf("unexpected value (%v)", e.Value)
	}

	panic("never reached")
}
