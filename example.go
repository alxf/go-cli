// +build ignore

// Show case of the cli package
package main

import (
	"fmt"

	"gitlab.com/alxf/go-cli"
)

func main() {
	var (
		optbool   bool
		optint    int
		optuint   uint
		optstring string
	)

	cmd := cli.NewCommand("example", "example command line parser", "0.1")
	if err := cmd.AddOption("-a",
		&cli.OptBool{
			Usage: "bool option example",
		}); err != nil {
		panic(err)
	}

	if err := cmd.AddOption("--bool",
		&cli.OptBool{
			Usage:   "bool option example",
			Aliases: []string{"-b"},
		}); err != nil {
		panic(err)
	}

	if err := cmd.AddOption(
		"--int",
		&cli.OptInt{
			Usage:   "integer option example with choices",
			Aliases: []string{"-i"},
			Choices: []string{"1", "42"},
		}); err != nil {
		panic(err)
	}

	if err := cmd.AddOption(
		"--string",
		&cli.OptString{
			Usage: "string option example with default value and " +
				"possible choices",
			Aliases: []string{"-s"},
			Default: "a string",
			Choices: []string{"a string", "another string"},
		},
	); err != nil {
		panic(err)
	}

	if err := cmd.AddOption(
		"--uint",
		&cli.OptUint{
			Usage:   "unsigned integer option with default",
			Aliases: []string{"-u"},
			Default: "99",
		},
	); err != nil {
		panic(err)
	}

	if err := cmd.AddExclusiveGroup("-a", "-b"); err != nil {
		panic(err)
	}
	if err := cmd.AddExclusiveGroup("-i", "-u"); err != nil {
		panic(err)
	}

	cmd.ParseOrExit()

	cmd.LookupBoolVar("--bool", &optbool)
	cmd.LookupIntVar("--int", &optint)
	cmd.LookupUintVar("--uint", &optuint)
	cmd.LookupStringVar("--string", &optstring)

	fmt.Println("Command line parser result:")
	fmt.Printf(" --bool    = %v\n", optbool)
	fmt.Printf(" --int     = %v\n", optint)
	fmt.Printf(" --uint    = %v\n", optuint)
	fmt.Printf(" --string  = %v\n", optstring)
}
