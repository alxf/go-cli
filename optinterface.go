package cli

func (o *OptBool) GetAliases() []string {
	return o.Aliases
}

func (o *OptBool) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptBool) GetDefault() string {
	return o.Default
}

func (o *OptBool) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptBool) GetSeen() bool {
	return o.seen
}

func (o *OptBool) GetUsage() string {
	return o.Usage
}

func (o *OptBool) GetValue() interface{} {
	return o.value
}

func (o *OptBool) SetSeen(value bool) {
	o.seen = value
}

func (o *OptInt) GetAliases() []string {
	return o.Aliases
}

func (o *OptInt) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptInt) GetDefault() string {
	return o.Default
}

func (o *OptInt) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptInt) GetSeen() bool {
	return o.seen
}

func (o *OptInt) GetUsage() string {
	return o.Usage
}

func (o *OptInt) GetValue() interface{} {
	return o.value
}

func (o *OptInt) SetSeen(value bool) {
	o.seen = value
}

func (o *OptInt8) GetAliases() []string {
	return o.Aliases
}

func (o *OptInt8) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptInt8) GetDefault() string {
	return o.Default
}

func (o *OptInt8) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptInt8) GetSeen() bool {
	return o.seen
}

func (o *OptInt8) GetUsage() string {
	return o.Usage
}

func (o *OptInt8) GetValue() interface{} {
	return o.value
}

func (o *OptInt8) SetSeen(value bool) {
	o.seen = value
}

func (o *OptInt16) GetAliases() []string {
	return o.Aliases
}

func (o *OptInt16) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptInt16) GetDefault() string {
	return o.Default
}

func (o *OptInt16) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptInt16) GetSeen() bool {
	return o.seen
}

func (o *OptInt16) GetUsage() string {
	return o.Usage
}

func (o *OptInt16) GetValue() interface{} {
	return o.value
}

func (o *OptInt16) SetSeen(value bool) {
	o.seen = value
}

func (o *OptInt32) GetAliases() []string {
	return o.Aliases
}

func (o *OptInt32) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptInt32) GetDefault() string {
	return o.Default
}

func (o *OptInt32) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptInt32) GetSeen() bool {
	return o.seen
}

func (o *OptInt32) GetUsage() string {
	return o.Usage
}

func (o *OptInt32) GetValue() interface{} {
	return o.value
}

func (o *OptInt32) SetSeen(value bool) {
	o.seen = value
}

func (o *OptInt64) GetAliases() []string {
	return o.Aliases
}

func (o *OptInt64) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptInt64) GetDefault() string {
	return o.Default
}

func (o *OptInt64) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptInt64) GetSeen() bool {
	return o.seen
}

func (o *OptInt64) GetUsage() string {
	return o.Usage
}

func (o *OptInt64) GetValue() interface{} {
	return o.value
}

func (o *OptInt64) SetSeen(value bool) {
	o.seen = value
}

func (o *OptUint) GetAliases() []string {
	return o.Aliases
}

func (o *OptUint) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptUint) GetDefault() string {
	return o.Default
}

func (o *OptUint) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptUint) GetSeen() bool {
	return o.seen
}

func (o *OptUint) GetUsage() string {
	return o.Usage
}

func (o *OptUint) GetValue() interface{} {
	return o.value
}

func (o *OptUint) SetSeen(value bool) {
	o.seen = value
}

func (o *OptUint8) GetAliases() []string {
	return o.Aliases
}

func (o *OptUint8) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptUint8) GetDefault() string {
	return o.Default
}

func (o *OptUint8) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptUint8) GetSeen() bool {
	return o.seen
}

func (o *OptUint8) GetUsage() string {
	return o.Usage
}

func (o *OptUint8) GetValue() interface{} {
	return o.value
}

func (o *OptUint8) SetSeen(value bool) {
	o.seen = value
}

func (o *OptUint16) GetAliases() []string {
	return o.Aliases
}

func (o *OptUint16) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptUint16) GetDefault() string {
	return o.Default
}

func (o *OptUint16) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptUint16) GetSeen() bool {
	return o.seen
}

func (o *OptUint16) GetUsage() string {
	return o.Usage
}

func (o *OptUint16) GetValue() interface{} {
	return o.value
}

func (o *OptUint16) SetSeen(value bool) {
	o.seen = value
}

func (o *OptUint32) GetAliases() []string {
	return o.Aliases
}

func (o *OptUint32) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptUint32) GetDefault() string {
	return o.Default
}

func (o *OptUint32) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptUint32) GetSeen() bool {
	return o.seen
}

func (o *OptUint32) GetUsage() string {
	return o.Usage
}

func (o *OptUint32) GetValue() interface{} {
	return o.value
}

func (o *OptUint32) SetSeen(value bool) {
	o.seen = value
}

func (o *OptUint64) GetAliases() []string {
	return o.Aliases
}

func (o *OptUint64) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptUint64) GetDefault() string {
	return o.Default
}

func (o *OptUint64) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptUint64) GetSeen() bool {
	return o.seen
}

func (o *OptUint64) GetUsage() string {
	return o.Usage
}

func (o *OptUint64) GetValue() interface{} {
	return o.value
}

func (o *OptUint64) SetSeen(value bool) {
	o.seen = value
}

func (o *OptString) GetAliases() []string {
	return o.Aliases
}

func (o *OptString) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptString) GetDefault() string {
	return o.Default
}

func (o *OptString) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptString) GetSeen() bool {
	return o.seen
}

func (o *OptString) GetUsage() string {
	return o.Usage
}

func (o *OptString) GetValue() interface{} {
	return o.value
}

func (o *OptString) SetSeen(value bool) {
	o.seen = value
}

func (o *OptFloat32) GetAliases() []string {
	return o.Aliases
}

func (o *OptFloat32) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptFloat32) GetDefault() string {
	return o.Default
}

func (o *OptFloat32) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptFloat32) GetSeen() bool {
	return o.seen
}

func (o *OptFloat32) GetUsage() string {
	return o.Usage
}

func (o *OptFloat32) GetValue() interface{} {
	return o.value
}

func (o *OptFloat32) SetSeen(value bool) {
	o.seen = value
}

func (o *OptFloat64) GetAliases() []string {
	return o.Aliases
}

func (o *OptFloat64) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptFloat64) GetDefault() string {
	return o.Default
}

func (o *OptFloat64) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptFloat64) GetSeen() bool {
	return o.seen
}

func (o *OptFloat64) GetUsage() string {
	return o.Usage
}

func (o *OptFloat64) GetValue() interface{} {
	return o.value
}

func (o *OptFloat64) SetSeen(value bool) {
	o.seen = value
}

func (o *OptIP) GetAliases() []string {
	return o.Aliases
}

func (o *OptIP) isValidChoice(choice string) bool {
	if len(o.Choices) < 1 {
		return true
	}

	found := false
	for _, e := range o.Choices {
		if e == choice {
			found = true
			break
		}
	}

	return found
}

func (o *OptIP) GetDefault() string {
	return o.Default
}

func (o *OptIP) GetMandatory() bool {
	return o.Mandatory
}

func (o *OptIP) GetSeen() bool {
	return o.seen
}

func (o *OptIP) GetUsage() string {
	return o.Usage
}

func (o *OptIP) GetValue() interface{} {
	return o.value
}

func (o *OptIP) SetSeen(value bool) {
	o.seen = value
}
