package cli

import (
	"fmt"
	"net"
	"strconv"
)

// IOption interface to implement for option types, user should be able to
// extend the parser with custom options using this interface
type IOption interface {
	GetAliases() []string
	GetDefault() string
	GetMandatory() bool
	GetSeen() bool
	GetUsage() string
	GetValue() interface{}
	SetSeen(bool)
	SetValue(interface{}) error
	isValidChoice(string) bool
}

// In order to add a new type of option, you need to:
//
// - edit cligen.py to add the the namedtuple corresponding to the new type
// - add the SetValue function interface to this type
// - set the fallback value if no default given for the new type in the
//   AddOption function in `cli.go`
// - add the new type to the parser function in `parser.go`
// - add the new type to the parser function in `subcommand.go`
// - add the new type to the formatOption function `common.go`
// - add few unittests
//

//go:generate ./cligen.py struct

//go:generate ./cligen.py interface

// SetValue function to set the option value from the native type
// (bool) or a string
func (o *OptBool) SetValue(value interface{}) error {
	switch value.(type) {
	case bool:
		o.value = value.(bool)
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		b, err := strconv.ParseBool(value.(string))
		if err != nil {
			return &ValueError{Kind: InvalidValue, Value: value}
		}
		o.value = b
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (int) or a string
func (o *OptInt) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case int:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(int)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseInt(value.(string), 10, 0)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = int(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (int8) or string
func (o *OptInt8) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case int8:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(int8)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseInt(value.(string), 10, 8)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = int8(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (int16) or string
func (o *OptInt16) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case int16:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(int16)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseInt(value.(string), 10, 16)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = int16(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (int32) or string
func (o *OptInt32) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case int32:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(int32)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseInt(value.(string), 10, 32)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = int32(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (int64) or string
func (o *OptInt64) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case int64:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(int64)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseInt(value.(string), 10, 64)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = int64(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (uint) or a string
func (o *OptUint) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case uint:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(uint)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseUint(value.(string), 10, 0)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = uint(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (uint8) or a string
func (o *OptUint8) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case uint8:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(uint8)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseUint(value.(string), 10, 8)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = uint8(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (uint16) or a string
func (o *OptUint16) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case uint16:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(uint16)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseUint(value.(string), 10, 16)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = uint16(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (uint32) or a string
func (o *OptUint32) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case uint32:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(uint32)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseUint(value.(string), 10, 32)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = uint32(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (uint64) or a string
func (o *OptUint64) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case uint64:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(uint64)
		}
	case string:
		if value.(string) == "" { // empty value, ignore it
			return nil
		}
		if o.isValidChoice(value.(string)) {
			i, err := strconv.ParseUint(value.(string), 10, 64)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = uint64(i)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value (only string which is the
// native type)
func (o *OptString) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case string:
		if o.isValidChoice(value.(string)) {
			found = true
			o.value = value.(string)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (float32) or a string
func (o *OptFloat32) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case float32:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(float32)
		}
	case string:
		if value.(string) == "" {
			return nil
		}
		if o.isValidChoice(value.(string)) {
			f, err := strconv.ParseFloat(value.(string), 32)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = float32(f)
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (float64) or a string
func (o *OptFloat64) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case float64:
		if o.isValidChoice(fmt.Sprintf("%v", value)) {
			found = true
			o.value = value.(float64)
		}
	case string:
		if value.(string) == "" {
			return nil
		}
		if o.isValidChoice(value.(string)) {
			f, err := strconv.ParseFloat(value.(string), 64)
			if err != nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = f
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}

// SetValue function to set the option value from the native type
// (net.IP) or a string
func (o *OptIP) SetValue(value interface{}) error {
	var found bool

	switch value.(type) {
	case net.IP:
		if o.isValidChoice((fmt.Sprintf("%v", value))) {
			found = true
			o.value = value.(net.IP)
		}
	case string:
		if value.(string) == "" {
			return nil
		}
		if o.isValidChoice(value.(string)) {
			ip := net.ParseIP(value.(string))
			if ip == nil {
				return &ValueError{Kind: InvalidValue, Value: value}
			}
			found = true
			o.value = ip
		}
	default:
		return &ValueError{Kind: UnexpectedValue, Value: value}
	}

	if !found {
		return &ValueError{Kind: InvalidChoice, Value: value}
	}

	return nil
}
