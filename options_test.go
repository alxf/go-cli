package cli

import (
	"testing"
)

// Test mandatory options feature
func TestMandatoryOptions(t *testing.T) {
	var inputTests = []inputParseOptions{
		{
			"-m value --",
			nil,
			map[string]interface{}{
				"-m": "value",
			},
			[]string{},
		},
		{
			"--",
			&OptionError{Kind: OptionMandatory, Name: "-m"},
			map[string]interface{}{},
			[]string{},
		},
	}

	for _, input := range inputTests {
		cmd := func() *Command {
			c := NewCommand("test", "testing parser", "0.0")

			if err := c.AddOption("-m", &OptString{
				Mandatory: true,
				Usage:     "use the given string as description",
			}); err != nil {
				panic(err)
			}

			return c
		}()

		err := parseInput(t, cmd, input.command)
		assertEq(t, err, input.err)
		if err != nil {
			continue
		}

		for k, v := range input.options {
			assertEq(t, cmd.options[k].GetValue(), v)
		}

		assertEachEq(t, cmd.Args, input.args)
	}
}

// Test exclusive groups of options
func TestExclusiveGroups(t *testing.T) {
	var inputTests = []inputParseOptions{
		{
			"-a --",
			nil,
			map[string]interface{}{
				"-a": true,
				"-b": false,
			},
			[]string{},
		},
		{
			"-b --",
			nil,
			map[string]interface{}{
				"-a": false,
				"-b": true,
			},
			[]string{},
		},
		{
			"-a -b --",
			&OptionError{
				Kind:    OptionExclusive,
				Name:    "-b",
				ExcList: []string{"-a", "-b"},
			},
			map[string]interface{}{},
			[]string{},
		},
	}

	for _, input := range inputTests {
		cmd := func() *Command {
			c := NewCommand("test", "testing parser", "0.0")

			if err := c.AddOption("-a", &OptBool{}); err != nil {
				panic(err)
			}
			if err := c.AddOption("-b", &OptBool{}); err != nil {
				panic(err)
			}

			if err := c.AddExclusiveGroup("-a", "-b"); err != nil {
				panic(err)
			}

			return c
		}()

		err := parseInput(t, cmd, input.command)
		assertEq(t, err, input.err)
		if err != nil {
			continue
		}

		for k, v := range input.options {
			assertEq(t, cmd.options[k].GetValue(), v)
		}

		assertEachEq(t, cmd.Args, input.args)
	}
}
