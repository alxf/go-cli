package cli

import (
	"net"
)

func (c *Command) LookupBoolVar(key string, value *bool) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(bool)
}

func (c *Command) LookupIntVar(key string, value *int) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int)
}

func (c *Command) LookupInt8Var(key string, value *int8) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int8)
}

func (c *Command) LookupInt16Var(key string, value *int16) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int16)
}

func (c *Command) LookupInt32Var(key string, value *int32) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int32)
}

func (c *Command) LookupInt64Var(key string, value *int64) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int64)
}

func (c *Command) LookupUintVar(key string, value *uint) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint)
}

func (c *Command) LookupUint8Var(key string, value *uint8) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint8)
}

func (c *Command) LookupUint16Var(key string, value *uint16) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint16)
}

func (c *Command) LookupUint32Var(key string, value *uint32) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint32)
}

func (c *Command) LookupUint64Var(key string, value *uint64) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint64)
}

func (c *Command) LookupStringVar(key string, value *string) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(string)
}

func (c *Command) LookupFloat32Var(key string, value *float32) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(float32)
}

func (c *Command) LookupFloat64Var(key string, value *float64) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(float64)
}

func (c *Command) LookupIPVar(key string, value *net.IP) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(net.IP)
}
