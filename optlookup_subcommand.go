package cli

import (
	"net"
)

func (c *SubCommand) LookupBoolVar(key string, value *bool) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(bool)
}

func (c *SubCommand) LookupIntVar(key string, value *int) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int)
}

func (c *SubCommand) LookupInt8Var(key string, value *int8) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int8)
}

func (c *SubCommand) LookupInt16Var(key string, value *int16) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int16)
}

func (c *SubCommand) LookupInt32Var(key string, value *int32) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int32)
}

func (c *SubCommand) LookupInt64Var(key string, value *int64) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(int64)
}

func (c *SubCommand) LookupUintVar(key string, value *uint) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint)
}

func (c *SubCommand) LookupUint8Var(key string, value *uint8) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint8)
}

func (c *SubCommand) LookupUint16Var(key string, value *uint16) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint16)
}

func (c *SubCommand) LookupUint32Var(key string, value *uint32) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint32)
}

func (c *SubCommand) LookupUint64Var(key string, value *uint64) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(uint64)
}

func (c *SubCommand) LookupStringVar(key string, value *string) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(string)
}

func (c *SubCommand) LookupFloat32Var(key string, value *float32) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(float32)
}

func (c *SubCommand) LookupFloat64Var(key string, value *float64) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(float64)
}

func (c *SubCommand) LookupIPVar(key string, value *net.IP) {
	opt, ok := c.options[key]
	if !ok {
		return
	}
	*value = opt.GetValue().(net.IP)
}
