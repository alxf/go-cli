package cli

import (
	"net"
)

// OptBool ...
type OptBool struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     bool
}

// OptInt ...
type OptInt struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     int
}

// OptInt8 ...
type OptInt8 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     int8
}

// OptInt16 ...
type OptInt16 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     int16
}

// OptInt32 ...
type OptInt32 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     int32
}

// OptInt64 ...
type OptInt64 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     int64
}

// OptUint ...
type OptUint struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     uint
}

// OptUint8 ...
type OptUint8 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     uint8
}

// OptUint16 ...
type OptUint16 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     uint16
}

// OptUint32 ...
type OptUint32 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     uint32
}

// OptUint64 ...
type OptUint64 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     uint64
}

// OptString ...
type OptString struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     string
}

// OptFloat32 ...
type OptFloat32 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     float32
}

// OptFloat64 ...
type OptFloat64 struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     float64
}

// OptIP ...
type OptIP struct {
	Aliases   []string
	Choices   []string
	Default   string
	Mandatory bool
	Usage     string
	seen      bool
	value     net.IP
}
