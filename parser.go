package cli

import (
	"fmt"
	"os"
	"strings"
)

// Parse ...
func (c *Command) Parse(args ...string) error {
	var (
		key   string
		input = args
		skip  = false
	)

	if len(input) < 1 {
		input = os.Args[1:]
	}

parseLoop:
	for i := 0; i < len(input); i++ {
		if !skip { // here we are expecting option switch
			if strings.HasPrefix(input[i], "-") {
				// detect option delimiter and auto generated options first
				switch input[i] {
				case "--help", "-h":
					c.Usage()
					os.Exit(0)
				case "--version":
					fmt.Printf("%v v%v\n", c.Program, c.Version)
					os.Exit(0)
				case "--":
					c.Args = input[i+1:]
					break parseLoop
				}

				// try to find if the option exists (name or alias)
				key = c.findOptionKey(input[i])

				if key != "" { // we are good, valid option found
					switch c.options[key].(type) {
					case *OptBool:
						if err := c.setBoolOption(key); err != nil {
							return &OptionError{
								Kind: OptionInternalError,
								Err:  err,
							}
						}
					case *OptString, *OptIP:
						skip = true
					case *OptInt, *OptInt8, *OptInt16, *OptInt32, *OptInt64:
						skip = true
					case *OptUint, *OptUint8, *OptUint16, *OptUint32, *OptUint64:
						skip = true
					case *OptFloat32, *OptFloat64:
						skip = true
					default:
						return &OptionError{
							Kind: OptionUnexpectedType,
							Name: key,
						}
					}
					continue
				}

				// try to parse option of this format `--long-option=value`
				if strings.HasPrefix(input[i], "--") {
					var value string

					if key, value = splitLongOption(input[i]); key == "" {
						return &OptionError{
							Kind: OptionNotFound,
							Name: input[i],
						}
					}

					if err := c.setOption(key, value); err != nil {
						return &OptionError{
							Kind: OptionInternalError,
							Err:  err,
						}
					}
				}

				// try to parse options of this format `-abcde`
				flags := input[i][1:]

				if len(flags) < 2 { // if only one flag found, it's an
					return &OptionError{ // invalid flag at this point
						Kind: OptionNotFound,
						Name: input[i],
					}
				}

				for _, o := range flags {
					flagname := fmt.Sprintf("-%s", string(o))

					if key = c.findOptionKey(flagname); key == "" {
						return &OptionError{
							Kind: OptionNotFound,
							Name: flagname,
						}
					}

					// currently we support only bool flag here
					switch c.options[key].(type) {
					case *OptBool:
						if err := c.setBoolOption(key); err != nil {
							return &OptionError{
								Kind: OptionInternalError,
								Err:  err,
							}
						}
					default:
						return &OptionError{
							Kind: OptionUnexpectedType,
							Name: input[i],
						}
					}
				}
			} else {
				sp, ok := c.subcommands[input[i]]

				if !ok { // no subcommand detected
					c.Args = input[i:]
					break parseLoop
				}

				// subcommand found
				c.command = input[i]
				// delegate parsing to subcommand
				args, err := sp.Parse(input[i+1:]...)
				if err != nil {
					return err
				}
				c.Args = args
				break parseLoop
			}
		} else { // here we are expecting an option argument
			if input[i] == "--" {
				return &OptionError{Kind: OptionMissingArgument, Name: key}
			}
			skip = false
			if err := c.setOption(key, input[i]); err != nil {
				return err
			}
		}
	}

	if err := c.checkExclusiveGroups(); err != nil {
		return err
	}

	return c.checkMandatoryOptions()
}

// ParseOrExit ...
func (c *Command) ParseOrExit(args ...string) {
	if err := c.Parse(args...); err != nil {
		if _, ferr := fmt.Fprintf(os.Stderr, "[error] %v\n", err); ferr != nil {
			panic(ferr)
		}
		os.Exit(2)
	}
}
