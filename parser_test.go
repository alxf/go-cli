package cli

import (
	"net"
	"testing"

	"github.com/google/shlex"
)

func parseInput(t *testing.T, c *Command, value string) error {
	args, err := shlex.Split(value)
	if err != nil {
		t.Fatalf("shlex: problem occurs (%v)", err)
	}

	return c.Parse(args...)
}

type inputParseOptions struct {
	command string
	err     error
	options map[string]interface{}
	args    []string
}

// Test: Parsing short options /////////////////////////////////////////////////

func TestShortOptions(t *testing.T) {
	var inputTests = []inputParseOptions{
		{
			"-b -i 42 -u 7 -s ohé -f 5.3 -F -543.5 -I 192.168.1.42 --" +
				" oh la la",
			nil,
			map[string]interface{}{
				"-b": true,
				"-i": 42,
				"-u": uint(7),
				"-s": "ohé",
				"-f": float32(5.3),
				"-F": -543.5,
				"-I": net.IPv4(192, 168, 1, 42),
			},
			[]string{"oh", "la", "la"},
		},
		{
			"-- oh la la",
			nil,
			map[string]interface{}{
				"-b": false,
				"-i": 0,
				"-u": uint(0),
				"-s": "",
				"-f": float32(0.0),
				"-F": 0.0,
				"-I": net.IP([]byte{}),
			},
			[]string{"oh", "la", "la"},
		},
		{
			"-z",
			&OptionError{Kind: OptionNotFound, Name: "-z"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-u -1",
			&OptionError{Kind: OptionInvalidValue, Name: "-u"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-i abc",
			&OptionError{Kind: OptionInvalidValue, Name: "-i"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-I abc",
			&OptionError{Kind: OptionInvalidValue, Name: "-I"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-s -- oh la la",
			&OptionError{Kind: OptionMissingArgument, Name: "-s"},
			map[string]interface{}{},
			[]string{},
		},
	}

	for _, input := range inputTests {
		// setup command line parser
		cmd := func() *Command {
			c := NewCommand("test", "testing parser", "0.0")

			if err := c.AddOption("-b", &OptBool{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-i", &OptInt{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-u", &OptUint{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-s", &OptString{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-f", &OptFloat32{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-F", &OptFloat64{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-I", &OptIP{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			return c
		}()

		// execute test against parsing result
		err := parseInput(t, cmd, input.command)
		assertEq(t, err, input.err)
		if err != nil {
			continue
		}

		for k, v := range input.options {
			assertEq(t, cmd.options[k].GetValue(), v)
		}

		assertEachEq(t, cmd.Args, input.args)
	}
}

func TestShortIntOptions(t *testing.T) {
	var inputTests = []inputParseOptions{
		{
			"-a 8 -b 16 -c 32 -d 64 --",
			nil,
			map[string]interface{}{
				"-a": int8(8),
				"-b": int16(16),
				"-c": int32(32),
				"-d": int64(64),
			},
			[]string{},
		},
		{
			"--",
			nil,
			map[string]interface{}{
				"-a": int8(0),
				"-b": int16(0),
				"-c": int32(0),
				"-d": int64(0),
			},
			[]string{},
		},
		{
			"-a -129 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-a"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-a 128 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-a"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-b -32769 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-b"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-b 32768 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-b"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-c -2147483649 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-c"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-c 2147483648 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-c"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-d -9223372036854775809 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-d"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-d 9223372036854775808 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-d"},
			map[string]interface{}{},
			[]string{},
		},
	}

	for _, input := range inputTests {
		cmd := func() *Command {
			c := NewCommand("test", "testing parser", "0.0")

			if err := c.AddOption("-a", &OptInt8{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-b", &OptInt16{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-c", &OptInt32{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-d", &OptInt64{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			return c
		}()

		err := parseInput(t, cmd, input.command)
		assertEq(t, err, input.err)
		if err != nil {
			continue
		}

		for k, v := range input.options {
			assertEq(t, cmd.options[k].GetValue(), v)
		}

		assertEachEq(t, cmd.Args, input.args)
	}
}

func TestShortUintOptions(t *testing.T) {
	var inputTests = []inputParseOptions{
		{
			"-a 8 -b 16 -c 32 -d 64 --",
			nil,
			map[string]interface{}{
				"-a": uint8(8),
				"-b": uint16(16),
				"-c": uint32(32),
				"-d": uint64(64),
			},
			[]string{},
		},
		{
			"--",
			nil,
			map[string]interface{}{
				"-a": uint8(0),
				"-b": uint16(0),
				"-c": uint32(0),
				"-d": uint64(0),
			},
			[]string{},
		},
		{
			"-a -1 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-a"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-a 256 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-a"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-b -1 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-b"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-b 65536 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-b"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-c -1 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-c"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-c 4294967296 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-c"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-d -1 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-d"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"-d 18446744073709551616 --",
			&OptionError{Kind: OptionInvalidValue, Name: "-d"},
			map[string]interface{}{},
			[]string{},
		},
	}

	for _, input := range inputTests {
		cmd := func() *Command {
			c := NewCommand("test", "testing parser", "0.0")

			if err := c.AddOption("-a", &OptUint8{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-b", &OptUint16{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-c", &OptUint32{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("-d", &OptUint64{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			return c
		}()

		err := parseInput(t, cmd, input.command)
		assertEq(t, err, input.err)
		if err != nil {
			continue
		}

		for k, v := range input.options {
			assertEq(t, cmd.options[k].GetValue(), v)
		}

		assertEachEq(t, cmd.Args, input.args)
	}
}

// Test: Parsing long options //////////////////////////////////////////////////

func TestLongOptions(t *testing.T) {
	var inputTests = []inputParseOptions{
		{
			"--bool --int 42 --uint 7 --string ohé --float 5.3" +
				" --float64 -543.5 --ip 192.168.1.42" +
				" -- oh la la",
			nil,
			map[string]interface{}{
				"--bool":    true,
				"--int":     42,
				"--uint":    uint(7),
				"--string":  "ohé",
				"--float":   float32(5.3),
				"--float64": -543.5,
				"--ip":      net.IPv4(192, 168, 1, 42),
			},
			[]string{"oh", "la", "la"},
		},
		{
			"-- oh la la",
			nil,
			map[string]interface{}{
				"--bool":    false,
				"--int":     0,
				"--uint":    uint(0),
				"--string":  "",
				"--float":   float32(0.0),
				"--float64": 0.0,
				"--ip":      net.IP([]byte{}),
			},
			[]string{"oh", "la", "la"},
		},
		{
			"--not-an-option",
			&OptionError{Kind: OptionNotFound, Name: "--not-an-option"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"--uint -1",
			&OptionError{Kind: OptionInvalidValue, Name: "--uint"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"--int abc",
			&OptionError{Kind: OptionInvalidValue, Name: "--int"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"--ip abc",
			&OptionError{Kind: OptionInvalidValue, Name: "--ip"},
			map[string]interface{}{},
			[]string{},
		},
		{
			"--string -- oh la la",
			&OptionError{Kind: OptionMissingArgument, Name: "--string"},
			map[string]interface{}{},
			[]string{},
		},
	}

	for _, input := range inputTests {
		cmd := func() *Command {
			c := NewCommand("test", "testing parser", "0.0")

			if err := c.AddOption("--bool", &OptBool{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("--int", &OptInt{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("--uint", &OptUint{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("--string", &OptString{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("--float", &OptFloat32{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("--float64", &OptFloat64{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			if err := c.AddOption("--ip", &OptIP{}); err != nil {
				t.Fatalf("AddOption: problem occurs (%v)", err)
			}

			return c
		}()

		err := parseInput(t, cmd, input.command)
		assertEq(t, err, input.err)
		if err != nil {
			continue
		}

		for k, v := range input.options {
			assertEq(t, cmd.options[k].GetValue(), v)
		}

		assertEachEq(t, cmd.Args, input.args)
	}
}

// Test: Parsing arguments /////////////////////////////////////////////////////

type inputParseArgs struct {
	command string
	err     error
	args    []string
}

func TestParseArgs(t *testing.T) {
	var inputTests = []inputParseArgs{
		{
			"arg1 arg2 arg3",
			nil,
			[]string{"arg1", "arg2", "arg3"},
		},
		{
			"-- arg1 arg2 arg3",
			nil,
			[]string{"arg1", "arg2", "arg3"},
		},
		{
			"-- -a -b -c --def-ghi",
			nil,
			[]string{"-a", "-b", "-c", "--def-ghi"},
		},
	}

	cmd := NewCommand("test", "testing parser", "0.0")

	for _, input := range inputTests {
		err := parseInput(t, cmd, input.command)
		assertEq(t, err, input.err)
		if err != nil {
			continue
		}

		assertEachEq(t, cmd.Args, input.args)
	}
}
