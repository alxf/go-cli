package cli

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

// SubCommand structure which hold a sub command definition
type SubCommand struct {
	Program     string
	Aliases     []string
	Description string
	options     map[string]IOption
	exclusives  [][]string
}

// SubCommand implementation ///////////////////////////////////////////////////

// AddOption `SubCommand` method to add an option to the subparser
func (c *SubCommand) AddOption(name string, opt IOption) error {
	if _, ok := c.options[name]; ok {
		return &OptionError{Kind: OptionAlreadyExists, Name: name}
	}

	if err := opt.SetValue(opt.GetDefault()); err != nil {
		return err
	}
	opt.SetSeen(false)
	c.options[name] = opt

	return nil
}

// AddExclusiveGroup `SubCommand` method to add a group of exclusive
// options by name or alias
func (c *SubCommand) AddExclusiveGroup(args ...string) error {
	if len(args) < 2 {
		return fmt.Errorf(
			"exclusive group need at least 2 options to be useful",
		)
	}

	group := make([]string, 0)
	for _, opt := range args {
		name := c.findOptionKey(opt)
		if name == "" {
			return fmt.Errorf("invalid option name '%v'", opt)
		}
		group = append(group, name)
	}
	c.exclusives = append(c.exclusives, group)

	return nil
}

// Parse `SubCommand` method to parse options
func (c *SubCommand) Parse(args ...string) ([]string, error) {
	var (
		key    string
		input  = args
		skip   = false
		result []string
	)

parseLoop:
	for i := 0; i < len(input); i++ {
		if !skip { // here we are expecting option switch
			if strings.HasPrefix(input[i], "-") {
				switch input[i] {
				case "--help", "-h":
					c.Usage()
					os.Exit(0)
				case "--":
					result = input[i+1:]
					break parseLoop
				}

				// try to find if the option exists (name or alias)
				key = c.findOptionKey(input[i])

				if key != "" { // we are good, valid option found
					switch c.options[key].(type) {
					case *OptBool:
						if err := c.setBoolOption(key); err != nil {
							return []string{}, &OptionError{
								Kind: OptionInternalError,
								Err:  err,
							}
						}
					case *OptString, *OptIP:
						skip = true
					case *OptInt, *OptInt8, *OptInt16, *OptInt32, *OptInt64:
						skip = true
					case *OptUint, *OptUint8, *OptUint16, *OptUint32, *OptUint64:
						skip = true
					case *OptFloat32, *OptFloat64:
						skip = true
					default:
						return []string{}, &OptionError{
							Kind: OptionUnexpectedType,
							Name: key,
						}
					}
					continue
				}

				// try to parse option of this format `--long-option=value`
				if strings.HasPrefix(input[i], "--") {
					var value string

					if key, value = splitLongOption(input[i]); key == "" {
						return []string{}, &OptionError{
							Kind: OptionNotFound,
							Name: input[i],
						}
					}

					if err := c.setOption(key, value); err != nil {
						return []string{}, &OptionError{
							Kind: OptionInternalError,
							Err:  err,
						}
					}
				}

				// try to parse options of this format `-abcdeF`
				flags := input[i][1:]

				if len(flags) < 2 {
					return []string{}, &OptionError{
						Kind: OptionNotFound,
						Name: input[i],
					}
				}

				for _, o := range flags {
					flagname := fmt.Sprintf("-%s", string(o))

					if key = c.findOptionKey(flagname); key == "" {
						return []string{}, &OptionError{
							Kind: OptionNotFound,
							Name: flagname,
						}
					}

					// currently only bool flag are supported here
					switch c.options[key].(type) {
					case *OptBool:
						if err := c.setBoolOption(key); err != nil {
							return []string{}, &OptionError{
								Kind: OptionInternalError,
								Err:  err,
							}
						}
					default:
						return []string{}, &OptionError{
							Kind: OptionUnexpectedType,
							Name: flagname,
						}
					}
				}
			} else {
				result = input[i:]
				break parseLoop
			}
		} else {
			if input[i] == "--" {
				return []string{}, &OptionError{
					Kind: OptionMissingArgument,
					Name: key,
				}
			}
			skip = false
			if err := c.setOption(key, input[i]); err != nil {
				return []string{}, err
			}
		}
	}

	if err := c.checkExclusiveGroups(); err != nil {
		return []string{}, err
	}
	if err := c.checkMandatoryOptions(); err != nil {
		return []string{}, err
	}

	return result, nil
}

// Usage `SubCommand` method to display usage for the subcommand only
func (c *SubCommand) Usage() {
	// TODO show usage for the given subcommand only
}

//go:generate ./cligen.py lookup SubCommand

// SubCommand helpers //////////////////////////////////////////////////////////

// findOptionKey `SubCommand` method to retrieve an option key given
// the name or an alias
func (c *SubCommand) findOptionKey(name string) string {
	if _, ok := c.options[name]; ok {
		return name
	}

	for k, v := range c.options {
		for _, s := range v.GetAliases() {
			if s == name {
				return k
			}
		}
	}

	return ""
}

// checkExclusiveGroups ...
func (c *SubCommand) checkExclusiveGroups() error {
	for _, group := range c.exclusives {
		found := false
		for _, opt := range group {
			if c.options[opt].GetSeen() && !found {
				found = true
			} else if c.options[opt].GetSeen() {
				return &OptionError{
					Kind:    OptionExclusive,
					ExcList: group,
					Name:    opt,
				}
			}
		}
	}

	return nil
}

// checkMandatoryOptions ...
func (c *SubCommand) checkMandatoryOptions() error {
	for k, v := range c.options {
		if v.GetMandatory() && !v.GetSeen() {
			return &OptionError{Kind: OptionMandatory, Name: k}
		}
	}

	return nil
}

// setOption ...
func (c *SubCommand) setOption(name string, value string) error {
	opt, ok := c.options[name]
	if !ok {
		return &OptionError{Kind: OptionNotFound, Name: name}
	}
	opt.SetSeen(true)
	if err := opt.SetValue(value); err != nil {
		switch err.(*ValueError).Kind {
		case InvalidValue:
			return &OptionError{Kind: OptionInvalidValue, Name: name}
		case InvalidChoice:
			return &OptionError{Kind: OptionInvalidChoice, Name: name}
		case UnexpectedValue:
			return &OptionError{Kind: OptionInternalError, Err: err}
		}
	}

	return nil
}

// setBoolOption ...
func (c *SubCommand) setBoolOption(name string) error {
	opt, ok := c.options[name]
	if !ok {
		return &OptionError{Kind: OptionNotFound, Name: name}
	}
	b, err := strconv.ParseBool(opt.GetDefault())
	if err != nil {
		b = false
	}

	opt.SetSeen(true)
	if err := opt.SetValue(!b); err != nil {
		switch err.(*ValueError).Kind {
		case InvalidValue:
			return &OptionError{Kind: OptionInvalidValue, Name: name}
		case UnexpectedValue:
			return &OptionError{Kind: OptionInternalError, Err: err}
		}
	}

	return nil
}
