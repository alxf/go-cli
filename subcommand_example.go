//+build ignore

package main

import (
	"fmt"

	cli "gitlab.com/alxf/go-cli"
)

func main() {
	app := cli.NewCommand("test", "testing subcommand parser", "")

	if err := app.AddOption("--config", &cli.OptString{}); err != nil {
		panic(err)
	}

	if err := app.AddOption("--debug", &cli.OptBool{}); err != nil {
		panic(err)
	}

	sp, err := app.AddSubCommand("show", "", "s")
	if err != nil {
		panic(err)
	}

	if err := sp.AddOption("--force", &cli.OptBool{}); err != nil {
		panic(err)
	}

	app.ParseOrExit()

	name, sc := app.GetCommand()
	if name == "show" {
		var force bool
		sc.LookupBoolVar("--force", &force)
		fmt.Println(force)
	}
}
